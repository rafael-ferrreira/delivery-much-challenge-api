# **delivery-much-challenge-api** <img src="./images/recipe.jpg" align="right" width="300">

Api to receive a string that represents ingredients from a recipe and returns recipe link and gif

## *Technologies adopted*
* node v14.15.4
* semistandard (lint)
* Swagger
* Jest (tests)

## *Specifications*
* nodeJS chosen for being a high performance language.
* Swagger to help with endpoint documentation and ease of requests.
* Added CI/CD

## Environment variables
Add the following values ​​to the ".env" development and test files
```
RECIPEPUPPY_URL=http://www.recipepuppy.com/api/
GIPHY_URL=http://api.giphy.com/v1/gifs/search
GIPHY_API_TOKEN=pPiMNFkdnBt4wGmBiJ9YCryAw3lHJk98
GIPHY_API_LIMIT=1
```

## *Execute*
1. Open the terminal and execute the command `npm install` - (install modules) at the root of the project
2. Execute inside folder root this command `npm start`
3. Open `http://localhost:3000/documentation`

## *test*
* `npm test`
```
-------------------------|---------|----------|---------|---------|-------------------
File                     | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
-------------------------|---------|----------|---------|---------|-------------------
All files                |   86.89 |    66.67 |   88.89 |   86.89 |                   
 api/recipes/controllers |   89.66 |       75 |     100 |   89.66 |                   
  receipt-controller.js  |   89.66 |       75 |     100 |   89.66 | 27-29             
 api/recipes/routes      |     100 |      100 |     100 |     100 |                   
  routes.js              |     100 |      100 |     100 |     100 |                   
 api/recipes/services    |      80 |      100 |     100 |      80 |                   
  receipt-service.js     |      80 |      100 |     100 |      80 | 13,26             
 config                  |      80 |      100 |      50 |      80 |                   
  server.js              |      80 |      100 |      50 |      80 | 38-39             
 config/environment      |      90 |       50 |     100 |      90 |                   
  index.js               |      90 |       50 |     100 |      90 | 17                
-------------------------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        1.203 s, estimated 2 s
```

## *lint*
* `npm run eslint`

## Request  
* Verbo: `GET`  
* Path: `http://localhost:3000/recipes/?i=onions,mussels` 

## Response  
* Http Status: `200`

## Example 

```  
# result
{
    "keywords": [
        "onions",
        "mussels"
    ],
    "recipes": [
        {
            "title": "Steamed Mussels I",
            "ingredients": [
                "garlic",
                " mussels",
                " onions"
            ],
            "link": "http://allrecipes.com/Recipe/Steamed-Mussels-I/Detail.aspx",
            "gif": "https://giphy.com/gifs/winter-cold-hot-chocolate-JkKmOjJD5eDgYzqGuV"
        }
    ]
}
```

## Request ERROR without params
* Verbo: `GET`  
* Path: `http://localhost:3000/recipes/?` 

## Response  
* Http Status: `400`

## Example 

```  
# result
{
    "statusCode": 400,
    "error": "Bad Request",
    "message": "Invalid request query input"
}
```
## Request ERROR maximum of three ingredients
* Verbo: `GET`  
* Path: `http://localhost:3000/recipes/?i=onions,mussels,garlic,tomato` 

## Response  
* Http Status: `400`

## Example 

```  
# result
{
    "error": "You must submit a maximum of three ingredients"
}
```

