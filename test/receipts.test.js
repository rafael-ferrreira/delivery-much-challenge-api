/**
 * @jest-environment node
 */
/* global , expect, test, beforeAll, afterAll */
'use strict';

import srv from '../src/config/server.js';
import nock from 'nock';
import puppyMock from '../test/mocks/recipepuppy_mock.json';
import giphyMock from '../test/mocks/giphy_mock.json';

srv.init();
const server = srv.server;

beforeAll((done) => {
  server.events.on('start', () => {
    done();
  });
  nock.cleanAll();
  nock.disableNetConnect();
});

// Stop application after running the test case
afterAll((done) => {
  server.events.on('stop', () => {
    done();
  });
  server.stop();
});

test('should return 400 because there are no parameters', async function () {
  const options = {
    method: 'GET',
    url: '/recipes/'
  };
  const data = await server.inject(options);
  expect(data.statusCode).toBe(400);
});

test('should return 400 because because there are more than three parameters', async function () {
  const options = {
    method: 'GET',
    url: '/recipes/?i=onions,mussels,garlic,tomato'
  };
  const data = await server.inject(options);
  expect(data.statusCode).toBe(400);
});

test('should return 200 because external APIs successfully registered', async function () {
  nock('http://www.recipepuppy.com')
    .get('/api/?i=onions,mussels')
    .reply(200, puppyMock);

  nock('http://api.giphy.com')
    .get('/v1/gifs/search?q=Steamed Mussels I&api_key=pPiMNFkdnBt4wGmBiJ9YCryAw3lHJk98&limit=1')
    .reply(200, giphyMock);

  const options = {
    method: 'GET',
    url: '/recipes/?i=onions,mussels'
  };
  const data = await server.inject(options);
  expect(data.statusCode).toBe(200);
});
