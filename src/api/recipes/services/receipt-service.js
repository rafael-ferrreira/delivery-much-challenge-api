'use strict';
import axios from 'axios';
import Config from '../../../config/environment/index.js';

class RecipesService {
  /**
   * Method responsible for taking a recipe from an external api
   * @param {Object} params - Object with request data
   * @returns {Object} reply return payload object
  */
  async getRecipepuppy (params) {
    const url = Config.recipepuppy_url;

    // eslint-disable-next-line no-useless-catch
    try {
      return await axios.get(`${url}?i=${params}`);
    } catch (error) {
      throw error;
    }
  }

  /**
   * Method responsible for taking a recipe from an external api
   * @param {Object} params - Object with request data
   * @returns {Object} reply return payload object
  */
  async getGiphyGif (params) {
    const url = Config.giphy_url;
    const apiKey = Config.giphy_api_token;
    const limit = Config.giphy_api_limit;

    // eslint-disable-next-line no-useless-catch
    try {
      return await axios.get(`${url}?q=${params}&api_key=${apiKey}&limit=${limit}`);
    } catch (error) {
      throw error;
    }
  }
}

export default RecipesService;
