'use strict';

import Fs from 'fs';
import Path from 'path';
import Dotenv from 'dotenv';
const __dirname = Path.resolve();

const initEnvVariables = () => {
  // Set default node environment to development
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';

  const envPath = process.env.NODE_ENV === 'test' ? Path.join(__dirname, '/test/.env') : Path.join(__dirname, '.env');

  try {
    Fs.statSync(envPath);
    Dotenv.config({ path: envPath });
  } catch (err) { console.log(envPath + ' not found, load by environment variables'); }
};
initEnvVariables();

// All configurations will extend these options
// ============================================
const all = {
  recipepuppy_url: process.env.RECIPEPUPPY_URL,
  giphy_url: process.env.GIPHY_URL,
  giphy_api_token: process.env.GIPHY_API_TOKEN,
  giphy_api_limit: process.env.GIPHY_API_LIMIT

};

// Export the config object based on the NODE_ENV
// ==============================================
export default all;
